<?php

namespace App\Repository;

use App\Entity\Dispositifs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dispositifs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dispositifs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dispositifs[]    findAll()
 * @method Dispositifs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispositifsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dispositifs::class);
    }

    // /**
    //  * @return Dispositifs[] Returns an array of Dispositifs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dispositifs
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
