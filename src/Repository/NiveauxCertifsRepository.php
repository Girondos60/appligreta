<?php

namespace App\Repository;

use App\Entity\NiveauxCertifs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NiveauxCertifs|null find($id, $lockMode = null, $lockVersion = null)
 * @method NiveauxCertifs|null findOneBy(array $criteria, array $orderBy = null)
 * @method NiveauxCertifs[]    findAll()
 * @method NiveauxCertifs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NiveauxCertifsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NiveauxCertifs::class);
    }

    // /**
    //  * @return NiveauxCertifs[] Returns an array of NiveauxCertifs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NiveauxCertifs
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
