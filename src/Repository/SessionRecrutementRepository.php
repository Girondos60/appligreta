<?php

namespace App\Repository;

use App\Entity\SessionRecrutement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SessionRecrutement|null find($id, $lockMode = null, $lockVersion = null)
 * @method SessionRecrutement|null findOneBy(array $criteria, array $orderBy = null)
 * @method SessionRecrutement[]    findAll()
 * @method SessionRecrutement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionRecrutementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SessionRecrutement::class);
    }

    // /**
    //  * @return SessionRecrutement[] Returns an array of SessionRecrutement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SessionRecrutement
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
