<?php

namespace App\Repository;

use App\Entity\NiveauFormations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NiveauFormations|null find($id, $lockMode = null, $lockVersion = null)
 * @method NiveauFormations|null findOneBy(array $criteria, array $orderBy = null)
 * @method NiveauFormations[]    findAll()
 * @method NiveauFormations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NiveauFormationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NiveauFormations::class);
    }

    // /**
    //  * @return NiveauFormations[] Returns an array of NiveauFormations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NiveauFormations
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
