<?php

namespace App\Repository;

use App\Entity\SpCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpCategories[]    findAll()
 * @method SpCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpCategories::class);
    }

    // /**
    //  * @return SpCategories[] Returns an array of SpCategories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpCategories
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
