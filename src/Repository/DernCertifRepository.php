<?php

namespace App\Repository;

use App\Entity\DernCertif;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DernCertif|null find($id, $lockMode = null, $lockVersion = null)
 * @method DernCertif|null findOneBy(array $criteria, array $orderBy = null)
 * @method DernCertif[]    findAll()
 * @method DernCertif[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DernCertifRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DernCertif::class);
    }

    // /**
    //  * @return DernCertif[] Returns an array of DernCertif objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DernCertif
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
