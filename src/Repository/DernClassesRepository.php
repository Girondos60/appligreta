<?php

namespace App\Repository;

use App\Entity\DernClasses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DernClasses|null find($id, $lockMode = null, $lockVersion = null)
 * @method DernClasses|null findOneBy(array $criteria, array $orderBy = null)
 * @method DernClasses[]    findAll()
 * @method DernClasses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DernClassesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DernClasses::class);
    }

    // /**
    //  * @return DernClasses[] Returns an array of DernClasses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DernClasses
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
