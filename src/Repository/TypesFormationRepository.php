<?php

namespace App\Repository;

use App\Entity\TypesFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypesFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypesFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypesFormation[]    findAll()
 * @method TypesFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesFormationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypesFormation::class);
    }

    // /**
    //  * @return TypesFormation[] Returns an array of TypesFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypesFormation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
