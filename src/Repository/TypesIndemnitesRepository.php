<?php

namespace App\Repository;

use App\Entity\TypesIndemnites;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypesIndemnites|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypesIndemnites|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypesIndemnites[]    findAll()
 * @method TypesIndemnites[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesIndemnitesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypesIndemnites::class);
    }

    // /**
    //  * @return TypesIndemnites[] Returns an array of TypesIndemnites objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypesIndemnites
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
