<?php

namespace App\Entity;

use App\Repository\FichesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FichesRepository::class)
 */
class Fiches
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomNaiss;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nationalite;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $depNaiss;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $villNaiss;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paysNaiss;

    /**
     * @ORM\Column(type="integer")
     */
    private $dureeTransport;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbEnfants;

    /**
     * @ORM\Column(type="integer")
     */
    private $numSecu;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cIdent;

    /**
     * @ORM\Column(type="date")
     */
    private $valIdent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomUrg;

    /**
     * @ORM\Column(type="integer")
     */
    private $telUrg;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lienParente;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sortScol;

    /**
     * @ORM\Column(type="boolean")
     */
    private $benefForm;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomForm;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDernForm;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $spAdresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $spPoste;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $identPE;

    /**
     * @ORM\Column(type="date")
     */
    private $datePE;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $agencePE;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $autre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emploi1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emploi2;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cpf;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mntCpf;

    /**
     * @ORM\Column(type="boolean")
     */
    private $handicap;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $admNomForm;

    /**
     * @ORM\Column(type="date")
     */
    private $admDebForm;

    /**
     * @ORM\Column(type="date")
     */
    private $admFinForm;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $numProgre;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nomContact;

    /**
     * @ORM\ManyToMany(targetEntity=Permis::class, inversedBy="fiches")
     */
    private $permis;

    /**
     * @ORM\ManyToMany(targetEntity=Transports::class, inversedBy="fiches")
     */
    private $transport;

    /**
     * @ORM\ManyToOne(targetEntity=SituationFamiliale::class, inversedBy="fiches")
     */
    private $situationFamiliale;

    /**
     * @ORM\ManyToMany(targetEntity=Dispositifs::class, inversedBy="fiches")
     */
    private $dispositif;

    /**
     * @ORM\ManyToOne(targetEntity=NiveauFormations::class, inversedBy="fiches")
     */
    private $niveauFormation;

    /**
     * @ORM\ManyToMany(targetEntity=Remunerations::class, inversedBy="fiches")
     */
    private $remuneration;

    /**
     * @ORM\ManyToOne(targetEntity=SpCategories::class, inversedBy="fiches")
     */
    private $spCategorie;

    /**
     * @ORM\ManyToOne(targetEntity=TypesIndemnites::class, inversedBy="fiches")
     */
    private $typesIndemnite;

    /**
     * @ORM\ManyToOne(targetEntity=DernCertif::class, inversedBy="fiches")
     */
    private $dernCertif;

    /**
     * @ORM\ManyToOne(targetEntity=NiveauxCertifs::class, inversedBy="fiches")
     */
    private $niveauxCertif;

    /**
     * @ORM\ManyToOne(targetEntity=DernClasses::class, inversedBy="fiches")
     */
    private $dernClasse;

    /**
     * @ORM\ManyToOne(targetEntity=Contacts::class, inversedBy="fiches")
     */
    private $contact;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     */
    private $user;

    public function __construct()
    {
        $this->permis = new ArrayCollection();
        $this->transport = new ArrayCollection();
        $this->dispositif = new ArrayCollection();
        $this->remuneration = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomNaiss(): ?string
    {
        return $this->nomNaiss;
    }

    public function setNomNaiss(string $nomNaiss): self
    {
        $this->nomNaiss = $nomNaiss;

        return $this;
    }

    public function getNationalite(): ?string
    {
        return $this->nationalite;
    }

    public function setNationalite(string $nationalite): self
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    public function getDepNaiss(): ?string
    {
        return $this->depNaiss;
    }

    public function setDepNaiss(string $depNaiss): self
    {
        $this->depNaiss = $depNaiss;

        return $this;
    }

    public function getVillNaiss(): ?string
    {
        return $this->villNaiss;
    }

    public function setVillNaiss(string $villNaiss): self
    {
        $this->villNaiss = $villNaiss;

        return $this;
    }

    public function getPaysNaiss(): ?string
    {
        return $this->paysNaiss;
    }

    public function setPaysNaiss(string $paysNaiss): self
    {
        $this->paysNaiss = $paysNaiss;

        return $this;
    }

    public function getDureeTransport(): ?int
    {
        return $this->dureeTransport;
    }

    public function setDureeTransport(int $dureeTransport): self
    {
        $this->dureeTransport = $dureeTransport;

        return $this;
    }

    public function getNbEnfants(): ?int
    {
        return $this->nbEnfants;
    }

    public function setNbEnfants(int $nbEnfants): self
    {
        $this->nbEnfants = $nbEnfants;

        return $this;
    }

    public function getNumSecu(): ?int
    {
        return $this->numSecu;
    }

    public function setNumSecu(int $numSecu): self
    {
        $this->numSecu = $numSecu;

        return $this;
    }

    public function getCIdent(): ?string
    {
        return $this->cIdent;
    }

    public function setCIdent(string $cIdent): self
    {
        $this->cIdent = $cIdent;

        return $this;
    }

    public function getValIdent(): ?\DateTimeInterface
    {
        return $this->valIdent;
    }

    public function setValIdent(\DateTimeInterface $valIdent): self
    {
        $this->valIdent = $valIdent;

        return $this;
    }

    public function getNomUrg(): ?string
    {
        return $this->nomUrg;
    }

    public function setNomUrg(string $nomUrg): self
    {
        $this->nomUrg = $nomUrg;

        return $this;
    }

    public function getTelUrg(): ?int
    {
        return $this->telUrg;
    }

    public function setTelUrg(int $telUrg): self
    {
        $this->telUrg = $telUrg;

        return $this;
    }

    public function getLienParente(): ?string
    {
        return $this->lienParente;
    }

    public function setLienParente(string $lienParente): self
    {
        $this->lienParente = $lienParente;

        return $this;
    }

    public function getSortScol(): ?bool
    {
        return $this->sortScol;
    }

    public function setSortScol(bool $sortScol): self
    {
        $this->sortScol = $sortScol;

        return $this;
    }

    public function getBenefForm(): ?bool
    {
        return $this->benefForm;
    }

    public function setBenefForm(bool $benefForm): self
    {
        $this->benefForm = $benefForm;

        return $this;
    }

    public function getNomForm(): ?string
    {
        return $this->nomForm;
    }

    public function setNomForm(string $nomForm): self
    {
        $this->nomForm = $nomForm;

        return $this;
    }

    public function getDateDernForm(): ?\DateTimeInterface
    {
        return $this->dateDernForm;
    }

    public function setDateDernForm(\DateTimeInterface $dateDernForm): self
    {
        $this->dateDernForm = $dateDernForm;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getSpAdresse(): ?string
    {
        return $this->spAdresse;
    }

    public function setSpAdresse(string $spAdresse): self
    {
        $this->spAdresse = $spAdresse;

        return $this;
    }

    public function getSpPoste(): ?string
    {
        return $this->spPoste;
    }

    public function setSpPoste(string $spPoste): self
    {
        $this->spPoste = $spPoste;

        return $this;
    }

    public function getIdentPE(): ?string
    {
        return $this->identPE;
    }

    public function setIdentPE(string $identPE): self
    {
        $this->identPE = $identPE;

        return $this;
    }

    public function getDatePE(): ?\DateTimeInterface
    {
        return $this->datePE;
    }

    public function setDatePE(\DateTimeInterface $datePE): self
    {
        $this->datePE = $datePE;

        return $this;
    }

    public function getAgencePE(): ?string
    {
        return $this->agencePE;
    }

    public function setAgencePE(string $agencePE): self
    {
        $this->agencePE = $agencePE;

        return $this;
    }

    public function getAutre(): ?string
    {
        return $this->autre;
    }

    public function setAutre(string $autre): self
    {
        $this->autre = $autre;

        return $this;
    }

    public function getEmploi1(): ?string
    {
        return $this->emploi1;
    }

    public function setEmploi1(?string $emploi1): self
    {
        $this->emploi1 = $emploi1;

        return $this;
    }

    public function getEmploi2(): ?string
    {
        return $this->emploi2;
    }

    public function setEmploi2(?string $emploi2): self
    {
        $this->emploi2 = $emploi2;

        return $this;
    }

    public function getCpf(): ?bool
    {
        return $this->cpf;
    }

    public function setCpf(bool $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getMntCpf(): ?int
    {
        return $this->mntCpf;
    }

    public function setMntCpf(?int $mntCpf): self
    {
        $this->mntCpf = $mntCpf;

        return $this;
    }

    public function getHandicap(): ?bool
    {
        return $this->handicap;
    }

    public function setHandicap(bool $handicap): self
    {
        $this->handicap = $handicap;

        return $this;
    }

    public function getAdmNomForm(): ?string
    {
        return $this->admNomForm;
    }

    public function setAdmNomForm(string $admNomForm): self
    {
        $this->admNomForm = $admNomForm;

        return $this;
    }

    public function getAdmDebForm(): ?\DateTimeInterface
    {
        return $this->admDebForm;
    }

    public function setAdmDebForm(\DateTimeInterface $admDebForm): self
    {
        $this->admDebForm = $admDebForm;

        return $this;
    }

    public function getAdmFinForm(): ?\DateTimeInterface
    {
        return $this->admFinForm;
    }

    public function setAdmFinForm(\DateTimeInterface $admFinForm): self
    {
        $this->admFinForm = $admFinForm;

        return $this;
    }

    public function getNumProgre(): ?string
    {
        return $this->numProgre;
    }

    public function setNumProgre(string $numProgre): self
    {
        $this->numProgre = $numProgre;

        return $this;
    }

    public function getNomContact(): ?string
    {
        return $this->nomContact;
    }

    public function setNomContact(?string $nomContact): self
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    /**
     * @return Collection|Permis[]
     */
    public function getPermis(): Collection
    {
        return $this->permis;
    }

    public function addPermi(Permis $permi): self
    {
        if (!$this->permis->contains($permi)) {
            $this->permis[] = $permi;
        }

        return $this;
    }

    public function removePermi(Permis $permi): self
    {
        $this->permis->removeElement($permi);

        return $this;
    }

    /**
     * @return Collection|Transports[]
     */
    public function getTransport(): Collection
    {
        return $this->transport;
    }

    public function addTransport(Transports $transport): self
    {
        if (!$this->transport->contains($transport)) {
            $this->transport[] = $transport;
        }

        return $this;
    }

    public function removeTransport(Transports $transport): self
    {
        $this->transport->removeElement($transport);

        return $this;
    }

    public function getSituationFamiliale(): ?SituationFamiliale
    {
        return $this->situationFamiliale;
    }

    public function setSituationFamiliale(?SituationFamiliale $situationFamiliale): self
    {
        $this->situationFamiliale = $situationFamiliale;

        return $this;
    }

    /**
     * @return Collection|Dispositifs[]
     */
    public function getDispositif(): Collection
    {
        return $this->dispositif;
    }

    public function addDispositif(Dispositifs $dispositif): self
    {
        if (!$this->dispositif->contains($dispositif)) {
            $this->dispositif[] = $dispositif;
        }

        return $this;
    }

    public function removeDispositif(Dispositifs $dispositif): self
    {
        $this->dispositif->removeElement($dispositif);

        return $this;
    }

    public function getNiveauFormation(): ?NiveauFormations
    {
        return $this->niveauFormation;
    }

    public function setNiveauFormation(?NiveauFormations $niveauFormation): self
    {
        $this->niveauFormation = $niveauFormation;

        return $this;
    }

    /**
     * @return Collection|Remunerations[]
     */
    public function getRemuneration(): Collection
    {
        return $this->remuneration;
    }

    public function addRemuneration(Remunerations $remuneration): self
    {
        if (!$this->remuneration->contains($remuneration)) {
            $this->remuneration[] = $remuneration;
        }

        return $this;
    }

    public function removeRemuneration(Remunerations $remuneration): self
    {
        $this->remuneration->removeElement($remuneration);

        return $this;
    }

    public function getSpCategorie(): ?SpCategories
    {
        return $this->spCategorie;
    }

    public function setSpCategorie(?SpCategories $spCategorie): self
    {
        $this->spCategorie = $spCategorie;

        return $this;
    }

    public function getTypesIndemnite(): ?TypesIndemnites
    {
        return $this->typesIndemnite;
    }

    public function setTypesIndemnite(?TypesIndemnites $typesIndemnite): self
    {
        $this->typesIndemnite = $typesIndemnite;

        return $this;
    }

    public function getDernCertif(): ?DernCertif
    {
        return $this->dernCertif;
    }

    public function setDernCertif(?DernCertif $dernCertif): self
    {
        $this->dernCertif = $dernCertif;

        return $this;
    }

    public function getNiveauxCertif(): ?NiveauxCertifs
    {
        return $this->niveauxCertif;
    }

    public function setNiveauxCertif(?NiveauxCertifs $niveauxCertif): self
    {
        $this->niveauxCertif = $niveauxCertif;

        return $this;
    }

    public function getDernClasse(): ?DernClasses
    {
        return $this->dernClasse;
    }

    public function setDernClasse(?DernClasses $dernClasse): self
    {
        $this->dernClasse = $dernClasse;

        return $this;
    }

    public function getContact(): ?Contacts
    {
        return $this->contact;
    }

    public function setContact(?Contacts $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
