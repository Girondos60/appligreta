<?php

namespace App\Entity;

use App\Repository\FormationsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=FormationsRepository::class)
 * @Vich\Uploadable
 */
class Formations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NomForm;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $refInterne;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="featured_images", fileNameProperty="image")
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity=Domaines::class, inversedBy="formations")
     */
    private $domaines;

    /**
     * @ORM\ManyToOne(targetEntity=TypesFormation::class, inversedBy="formations")
     */
    private $TypesFormation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dureeCentre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dureeEntreprise;

    /**
     *@var \DateTime $createdAt;
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="date", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime $updateAt;
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Sessions::class, mappedBy="formation")
     */
    private $sessions;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="formation")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=NiveauxProfessionnels::class, inversedBy="formations")
     */
    private $niveauxProfessionnels;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $module;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $public;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $prerequis;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $nbParticipants;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $orgPedag;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $methodePedag;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $moyenPedag;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $modSuivi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $modEval;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $validation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $profilIntervenants;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $dateAcces;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modAcces;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=255)
     */
    private $lieu;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tarif;

    


    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->users = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->getNomForm();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomForm(): ?string
    {
        return $this->NomForm;
    }

    public function setNomForm(string $NomForm): self
    {
        $this->NomForm = $NomForm;

        return $this;
    }

    public function getRefInterne(): ?string
    {
        return $this->refInterne;
    }

    public function setRefInterne(?string $refInterne): self
    {
        $this->refInterne = $refInterne;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(?File $imageFile = null)
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getDomaines(): ?Domaines
    {
        return $this->domaines;
    }

    public function setDomaines(?Domaines $domaines): self
    {
        $this->domaines = $domaines;

        return $this;
    }

    public function getTypesFormation(): ?TypesFormation
    {
        return $this->TypesFormation;
    }

    public function setTypesFormation(?TypesFormation $TypesFormation): self
    {
        $this->TypesFormation = $TypesFormation;

        return $this;
    }

    public function getDureeCentre(): ?int
    {
        return $this->dureeCentre;
    }

    public function setDureeCentre(?int $dureeCentre): self
    {
        $this->dureeCentre = $dureeCentre;

        return $this;
    }

    public function getDureeEntreprise(): ?int
    {
        return $this->dureeEntreprise;
    }

    public function setDureeEntreprise(?int $dureeEntreprise): self
    {
        $this->dureeEntreprise = $dureeEntreprise;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Sessions[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Sessions $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setFormation($this);
        }

        return $this;
    }

    public function removeSession(Sessions $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getFormation() === $this) {
                $session->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addFormation($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeFormation($this);
        }

        return $this;
    }

    public function getNiveauxProfessionnels(): ?NiveauxProfessionnels
    {
        return $this->niveauxProfessionnels;
    }

    public function setNiveauxProfessionnels(?NiveauxProfessionnels $niveauxProfessionnels): self
    {
        $this->niveauxProfessionnels = $niveauxProfessionnels;

        return $this;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function setModule(?string $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getPublic(): ?string
    {
        return $this->public;
    }

    public function setPublic(?string $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getPrerequis(): ?string
    {
        return $this->prerequis;
    }

    public function setPrerequis(?string $prerequis): self
    {
        $this->prerequis = $prerequis;

        return $this;
    }

    public function getNbParticipants(): ?string
    {
        return $this->nbParticipants;
    }

    public function setNbParticipants(?string $nbParticipants): self
    {
        $this->nbParticipants = $nbParticipants;

        return $this;
    }

    public function getOrgPedag(): ?string
    {
        return $this->orgPedag;
    }

    public function setOrgPedag(?string $orgPedag): self
    {
        $this->orgPedag = $orgPedag;

        return $this;
    }

    public function getMethodePedag(): ?string
    {
        return $this->methodePedag;
    }

    public function setMethodePedag(?string $methodePedag): self
    {
        $this->methodePedag = $methodePedag;

        return $this;
    }

    public function getMoyenPedag(): ?string
    {
        return $this->moyenPedag;
    }

    public function setMoyenPedag(?string $moyenPedag): self
    {
        $this->moyenPedag = $moyenPedag;

        return $this;
    }

    public function getModSuivi(): ?string
    {
        return $this->modSuivi;
    }

    public function setModSuivi(?string $modSuivi): self
    {
        $this->modSuivi = $modSuivi;

        return $this;
    }

    public function getModEval(): ?string
    {
        return $this->modEval;
    }

    public function setModEval(?string $modEval): self
    {
        $this->modEval = $modEval;

        return $this;
    }

    public function getValidation(): ?string
    {
        return $this->validation;
    }

    public function setValidation(?string $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getProfilIntervenants(): ?string
    {
        return $this->profilIntervenants;
    }

    public function setProfilIntervenants(?string $profilIntervenants): self
    {
        $this->profilIntervenants = $profilIntervenants;

        return $this;
    }

    public function getDateAcces(): ?string
    {
        return $this->dateAcces;
    }

    public function setDateAcces(?string $dateAcces): self
    {
        $this->dateAcces = $dateAcces;

        return $this;
    }

    public function getModAcces(): ?string
    {
        return $this->modAcces;
    }

    public function setModAcces(?string $modAcces): self
    {
        $this->modAcces = $modAcces;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(?string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getTarif(): ?int
    {
        return $this->tarif;
    }

    public function setTarif(?int $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    
}
