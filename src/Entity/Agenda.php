<?php

namespace App\Entity;

use App\Repository\AgendaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgendaRepository::class)
 */
class Agenda
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomAgenda;

    /**
     * @ORM\OneToOne(targetEntity=Sessions::class, cascade={"persist", "remove"})
     */
    private $session;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="agenda")
     */
    private $cours;

    
    public function __construct()
    {
        $this->cours = new ArrayCollection();
        
    }

    public function __toString()
    {
        return $this->getNomAgenda();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomAgenda(): ?string
    {
        return $this->nomAgenda;
    }

    public function setNomAgenda(string $nomAgenda): self
    {
        $this->nomAgenda = $nomAgenda;

        return $this;
    }

    public function getSession(): ?Sessions
    {
        return $this->session;
    }

    public function setSession(?Sessions $session): self
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setAgenda($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getAgenda() === $this) {
                $cour->setAgenda(null);
            }
        }

        return $this;
    }

    
}
