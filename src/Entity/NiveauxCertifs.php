<?php

namespace App\Entity;

use App\Repository\NiveauxCertifsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NiveauxCertifsRepository::class)
 */
class NiveauxCertifs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libNiveau;

    /**
     * @ORM\OneToMany(targetEntity=Fiches::class, mappedBy="niveauxCertif")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibNiveau(): ?string
    {
        return $this->libNiveau;
    }

    public function setLibNiveau(string $libNiveau): self
    {
        $this->libNiveau = $libNiveau;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setNiveauxCertif($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            // set the owning side to null (unless already changed)
            if ($fich->getNiveauxCertif() === $this) {
                $fich->setNiveauxCertif(null);
            }
        }

        return $this;
    }

}
