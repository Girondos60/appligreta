<?php

namespace App\Entity;

use App\Repository\DernCertifRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DernCertifRepository::class)
 */
class DernCertif
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libCertif;

    /**
     * @ORM\OneToMany(targetEntity=Fiches::class, mappedBy="dernCertif")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibCertif(): ?string
    {
        return $this->libCertif;
    }

    public function setLibCertif(string $libCertif): self
    {
        $this->libCertif = $libCertif;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setDernCertif($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            // set the owning side to null (unless already changed)
            if ($fich->getDernCertif() === $this) {
                $fich->setDernCertif(null);
            }
        }

        return $this;
    }

   
}
