<?php

namespace App\Entity;

use App\Repository\RemunerationsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RemunerationsRepository::class)
 */
class Remunerations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $libRemu;

    /**
     * @ORM\ManyToMany(targetEntity=Fiches::class, mappedBy="remuneration")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibRemu(): ?string
    {
        return $this->libRemu;
    }

    public function setLibRemu(string $libRemu): self
    {
        $this->libRemu = $libRemu;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->addRemuneration($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            $fich->removeRemuneration($this);
        }

        return $this;
    }

   
}
