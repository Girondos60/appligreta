<?php

namespace App\Entity;

use App\Repository\TransportsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransportsRepository::class)
 */
class Transports
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libTransport;

    /**
     * @ORM\ManyToMany(targetEntity=Fiches::class, mappedBy="transport")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibTransport(): ?string
    {
        return $this->libTransport;
    }

    public function setLibTransport(string $libTransport): self
    {
        $this->libTransport = $libTransport;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->addTransport($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            $fich->removeTransport($this);
        }

        return $this;
    }

}
