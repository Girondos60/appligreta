<?php

namespace App\Entity;

use App\Repository\SallesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SallesRepository::class)
 */
class Salles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $numSalle;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $etage;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="salles")
     */
    private $cours;

    public function __construct()
    {
        $this->cours = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNumSalle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumSalle(): ?string
    {
        return $this->numSalle;
    }

    public function setNumSalle(string $numSalle): self
    {
        $this->numSalle = $numSalle;

        return $this;
    }

    public function getEtage(): ?string
    {
        return $this->etage;
    }

    public function setEtage(string $etage): self
    {
        $this->etage = $etage;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setSalles($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getSalles() === $this) {
                $cour->setSalles(null);
            }
        }

        return $this;
    }
}
