<?php

namespace App\Entity;

use App\Repository\TypesIndemnitesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypesIndemnitesRepository::class)
 */
class TypesIndemnites
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $libType;

    /**
     * @ORM\OneToMany(targetEntity=Fiches::class, mappedBy="typesIndemnite")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibType(): ?string
    {
        return $this->libType;
    }

    public function setLibType(string $libType): self
    {
        $this->libType = $libType;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setTypesIndemnite($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            // set the owning side to null (unless already changed)
            if ($fich->getTypesIndemnite() === $this) {
                $fich->setTypesIndemnite(null);
            }
        }

        return $this;
    }

}
