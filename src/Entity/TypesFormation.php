<?php

namespace App\Entity;

use App\Repository\TypesFormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypesFormationRepository::class)
 */
class TypesFormation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $LibType;

    /**
     * @ORM\OneToMany(targetEntity=Formations::class, mappedBy="TypesFormation")
     */
    private $formations;

    public function __toString()
    {
        return $this->getLibType();
    }

    public function __construct()
    {
        $this->formations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibType(): ?string
    {
        return $this->LibType;
    }

    public function setLibType(string $LibType): self
    {
        $this->LibType = $LibType;

        return $this;
    }

    /**
     * @return Collection|Formations[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formations $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setTypesFormation($this);
        }

        return $this;
    }

    public function removeFormation(Formations $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getTypesFormation() === $this) {
                $formation->setTypesFormation(null);
            }
        }

        return $this;
    }
}
