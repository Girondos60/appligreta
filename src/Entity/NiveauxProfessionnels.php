<?php

namespace App\Entity;

use App\Repository\NiveauxProfessionnelsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NiveauxProfessionnelsRepository::class)
 */
class NiveauxProfessionnels
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libNiveau;

    /**
     * @ORM\OneToMany(targetEntity=Formations::class, mappedBy="niveauxProfessionnels")
     */
    private $formations;

    public function __toString()
    {
        return $this->getLibNiveau();
    }

    public function __construct()
    {
        $this->formations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibNiveau(): ?string
    {
        return $this->libNiveau;
    }

    public function setLibNiveau(?string $libNiveau): self
    {
        $this->libNiveau = $libNiveau;

        return $this;
    }

    /**
     * @return Collection|Formations[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formations $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setNiveauxProfessionnels($this);
        }

        return $this;
    }

    public function removeFormation(Formations $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getNiveauxProfessionnels() === $this) {
                $formation->setNiveauxProfessionnels(null);
            }
        }

        return $this;
    }
}
