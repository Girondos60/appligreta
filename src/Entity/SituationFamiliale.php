<?php

namespace App\Entity;

use App\Repository\SituationFamilialeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SituationFamilialeRepository::class)
 */
class SituationFamiliale
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libSituation;

    /**
     * @ORM\OneToMany(targetEntity=Fiches::class, mappedBy="situationFamiliale")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibSituation(): ?string
    {
        return $this->libSituation;
    }

    public function setLibSituation(string $libSituation): self
    {
        $this->libSituation = $libSituation;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setSituationFamiliale($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            // set the owning side to null (unless already changed)
            if ($fich->getSituationFamiliale() === $this) {
                $fich->setSituationFamiliale(null);
            }
        }

        return $this;
    }

}
