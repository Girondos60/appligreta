<?php

namespace App\Entity;

use App\Repository\DernClassesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DernClassesRepository::class)
 */
class DernClasses
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libClasse;

    /**
     * @ORM\OneToMany(targetEntity=Fiches::class, mappedBy="dernClasse")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibClasse(): ?string
    {
        return $this->libClasse;
    }

    public function setLibClasse(string $libClasse): self
    {
        $this->libClasse = $libClasse;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setDernClasse($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            // set the owning side to null (unless already changed)
            if ($fich->getDernClasse() === $this) {
                $fich->setDernClasse(null);
            }
        }

        return $this;
    }

}
