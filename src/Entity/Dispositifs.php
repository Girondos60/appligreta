<?php

namespace App\Entity;

use App\Repository\DispositifsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DispositifsRepository::class)
 */
class Dispositifs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $libDispositif;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $dispAutre;

    /**
     * @ORM\ManyToMany(targetEntity=Fiches::class, mappedBy="dispositif")
     */
    private $fiches;

    public function __construct()
    {
        $this->fiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibDispositif(): ?string
    {
        return $this->libDispositif;
    }

    public function setLibDispositif(string $libDispositif): self
    {
        $this->libDispositif = $libDispositif;

        return $this;
    }

    public function getDispAutre(): ?string
    {
        return $this->dispAutre;
    }

    public function setDispAutre(?string $dispAutre): self
    {
        $this->dispAutre = $dispAutre;

        return $this;
    }

    /**
     * @return Collection|Fiches[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiches $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->addDispositif($this);
        }

        return $this;
    }

    public function removeFich(Fiches $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            $fich->removeDispositif($this);
        }

        return $this;
    }

}
