<?php

namespace App\Controller;

use App\Entity\NiveauxProfessionnels;
use App\Form\NiveauxProfessionnelsType;
use App\Repository\NiveauxProfessionnelsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("module/admin/niveaux/professionnels")
 */
class NiveauxProfessionnelsController extends AbstractController
{
    /**
     * @Route("/", name="niveaux_professionnels_index", methods={"GET"})
     */
    public function index(NiveauxProfessionnelsRepository $niveauxProfessionnelsRepository): Response
    {
        return $this->render('niveaux_professionnels/index.html.twig', [
            'niveaux_professionnels' => $niveauxProfessionnelsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="niveaux_professionnels_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $niveauxProfessionnel = new NiveauxProfessionnels();
        $form = $this->createForm(NiveauxProfessionnelsType::class, $niveauxProfessionnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($niveauxProfessionnel);
            $entityManager->flush();

            return $this->redirectToRoute('niveaux_professionnels_index');
        }

        return $this->render('niveaux_professionnels/new.html.twig', [
            'niveaux_professionnel' => $niveauxProfessionnel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="niveaux_professionnels_show", methods={"GET"})
     */
    public function show(NiveauxProfessionnels $niveauxProfessionnel): Response
    {
        return $this->render('niveaux_professionnels/show.html.twig', [
            'niveaux_professionnel' => $niveauxProfessionnel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="niveaux_professionnels_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NiveauxProfessionnels $niveauxProfessionnel): Response
    {
        $form = $this->createForm(NiveauxProfessionnelsType::class, $niveauxProfessionnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('niveaux_professionnels_index');
        }

        return $this->render('niveaux_professionnels/edit.html.twig', [
            'niveaux_professionnel' => $niveauxProfessionnel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="niveaux_professionnels_delete", methods={"DELETE"})
     */
    public function delete(Request $request, NiveauxProfessionnels $niveauxProfessionnel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$niveauxProfessionnel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($niveauxProfessionnel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('niveaux_professionnels_index');
    }
}
