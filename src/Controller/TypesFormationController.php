<?php

namespace App\Controller;

use App\Entity\TypesFormation;
use App\Form\TypesFormationType;
use App\Repository\TypesFormationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("module/admin/types/formation")
 */
class TypesFormationController extends AbstractController
{
    /**
     * @Route("/", name="types_formation_index", methods={"GET"})
     */
    public function index(TypesFormationRepository $typesFormationRepository): Response
    {
        return $this->render('types_formation/index.html.twig', [
            'types_formations' => $typesFormationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="types_formation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typesFormation = new TypesFormation();
        $form = $this->createForm(TypesFormationType::class, $typesFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typesFormation);
            $entityManager->flush();

            return $this->redirectToRoute('types_formation_index');
        }

        return $this->render('types_formation/new.html.twig', [
            'types_formation' => $typesFormation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="types_formation_show", methods={"GET"})
     */
    public function show(TypesFormation $typesFormation): Response
    {
        return $this->render('types_formation/show.html.twig', [
            'types_formation' => $typesFormation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="types_formation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypesFormation $typesFormation): Response
    {
        $form = $this->createForm(TypesFormationType::class, $typesFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('types_formation_index');
        }

        return $this->render('types_formation/edit.html.twig', [
            'types_formation' => $typesFormation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="types_formation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypesFormation $typesFormation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typesFormation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typesFormation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('types_formation_index');
    }
}
