<?php

namespace App\Controller\Admin;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ModuleAdminController extends AbstractController
{

    protected $userRepository;
   
    
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        
    }

    /**
     * @Route("/module/admin", name="module_admin")
     */
    public function index(): Response
    {
        return $this->render('moduleadmin/index.html.twig', [
            'controller_name' => 'ModuleAdminController',
            'totalAllUsers' => $this->userRepository->countAllUsers(),
            
        ]);
    }

}
