<?php

namespace App\Controller;

use App\Entity\Domaines;
use App\Form\DomainesType;
use App\Repository\DomainesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("module/admin/domaines")
 */
class DomainesController extends AbstractController
{
    /**
     * @Route("/", name="domaines_index", methods={"GET"})
     */
    public function index(DomainesRepository $domainesRepository): Response
    {
        return $this->render('domaines/index.html.twig', [
            'domaines' => $domainesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="domaines_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $domaine = new Domaines();
        $form = $this->createForm(DomainesType::class, $domaine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($domaine);
            $entityManager->flush();

            return $this->redirectToRoute('domaines_index');
        }

        return $this->render('domaines/new.html.twig', [
            'domaine' => $domaine,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="domaines_show", methods={"GET"})
     */
    public function show(Domaines $domaine): Response
    {
        return $this->render('domaines/show.html.twig', [
            'domaine' => $domaine,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="domaines_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Domaines $domaine): Response
    {
        $form = $this->createForm(DomainesType::class, $domaine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('domaines_index');
        }

        return $this->render('domaines/edit.html.twig', [
            'domaine' => $domaine,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="domaines_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Domaines $domaine): Response
    {
        if ($this->isCsrfTokenValid('delete'.$domaine->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($domaine);
            $entityManager->flush();
        }

        return $this->redirectToRoute('domaines_index');
    }
}
