<?php

namespace App\Controller;
use App\Entity\User;
use App\Entity\Domaines;
use App\Entity\Sessions;
use App\Entity\Formations;
use App\Entity\Candidature;
use App\Entity\Fiches;
use App\Form\UserType;
use App\Form\FormationsType;
use App\Form\FicheType;
use App\Form\CandidatureType;
use App\Form\SessionsType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Repository\DomainesRepository;
use App\Repository\FormationsRepository;
use App\Repository\SessionsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class GretaController extends AbstractController
{
    /**
     * @Route("/greta", name="greta")
     */
    public function index(): Response
    {
        return $this->render('greta/index.html.twig', [
            'controller_name' => 'GretaController',
        ]);
    }

     /**
     * @Route("/", name="home")
     * @Method({"GET"})
     */
    public function home(){

        $repoDom = $this->getDoctrine()->getRepository(Domaines::class);
        $repoForm = $this->getDoctrine()->getRepository(Formations::class);
        $repoUser = $this->getDoctrine()->getRepository(User::class);


        $domaines = $repoDom->findAll();
        $formations = $repoForm->findAll();
        $users = $repoUser->findAll();


        return $this->render('greta/home.html.twig', [
            'controller_name' => 'GretaController',
            'domaines' => $domaines,
            'formations'=>$formations,
            'users' => $users,


        ]);
    }

    /**
     * @Route("/home/formations/{id}", name="formations")
     * @Method({"GET"})
     */
    public function formations(Domaines $domaines)
    {


         return $this->render('greta/formations.html.twig',[
            'controller_name' => 'GretaController',
            'domaines' => $domaines
        ]);

    }

     /**
     * @Route("/home/formationdetails/{id}", name="formationdetails")
     * @Method({"GET"})
     */
    public function formationdetails(Formations $formation)
    {

        return $this->render('greta/formationdetails.html.twig', [
            'controller_name' => 'GretaController',
            'formation'=>$formation

        ]);

    }

     /**
     * @Route("/home/formsess/{id}", name="formsess")
     * @Method({"GET"})
     */
    public function formsess(Request $request , Formations $formsess)
    {
      dump($formsess);
      $user = $this->getUser();
      $user->getFiche() ? $fiche = $user->getFiche() : $fiche = new Fiches();
       if (!$fiche->getUser()) $fiche->setUser($user);

      $form = $this->createForm(FicheType::class, $fiche , ["formation" => $formsess]);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid() && $this->getRequest()->isMethod('POST')){
          $candidature = new Candidature();
          $candidature->setDateCreation(new \DateTime('now'));
          $candidature->setStatut("En cours de traitement");
          $candidature->setCandidat($user);

          $manager->persist($candidature);
          $manager->persist($fiche);
          $manager->flush();
          $this->addFlash('success', 'Votre candidature a bien été enregistrée');
          return $this->redirectToRoute('home');

      }

      return $this->render('greta/formsess.html.twig', [
            'controller_name' => 'GretaController',
            'form' => $form->createView(),
            'formsess'=>$formsess,



        ]);

    }



    /**
     * @Route ("/login/newuser", name="user_new")
     * @Route ("/login/{id}/edit", name="user_edit")
     * @Method({"GET"})
     */
    public function formUser(User $user = null, Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder){


        if(!$user){
            $user = new User();
            $user->setRoles(["ROLE_CANDIDAT"]);

        }
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            if(!$user->getId()){
                $hash = $encoder->encodePassword($user, $form->get('plainPassword')->getData());
                $user->setPassword($hash);

            }
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'Votre inscription a bien été effectuée');
            return $this->redirectToRoute('app_login');

        }

        return $this->render('greta/newUser.html.twig', [
            'form' => $form->createView(),
            'editMode'=>$user->getId() !== null
        ]);
    }

    /**
     * @Route("log/{id}", name="user_show", methods={"GET"})
     *
     */
    public function show(User $user): Response
    {
        //repoUser = $this->getDoctrine()->getRepository(User::class);
        //$user = $repoUser->findAll();

        return $this->render('greta/showUser.html.twig', [
            'user' => $user,
        ]);
    }

}
