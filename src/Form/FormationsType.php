<?php

namespace App\Form;

use App\Entity\Formations;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class FormationsType extends AbstractType

/**
     * {@inheritdoc}
     */
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('NomForm', TextType::class, ['attr' => [
                'placeholder' => "ex: Gestionnaire de paye",
            ], 'label' => "NomForm *"])

            ->add('module')
            ->add('refInterne')
            ->add('description')
            ->add('imageFile', VichImageType::class, [
            'required' => false,
            'allow_delete' => true,
            'delete_label' => 'Supprimer',
            'download_label' => 'Télécharger',
            'download_uri' => true,
            'image_uri' => true,
            'asset_helper' => true,
        ])
            ->add('dureeCentre')
            ->add('dureeEntreprise')
            ->add('domaines')
            ->add('TypesFormation')
            ->add('niveauxProfessionnels')
            ->add('public', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('prerequis',TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('nbParticipants', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('orgPedag', TextareaType::class, ['attr' => ['maxlength'=> 255]])
            ->add('methodePedag', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('moyenPedag', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('modSuivi', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('modEval', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('validation', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('profilIntervenants', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('dateAcces', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('modAcces', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('lieu', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('tarif', TextareaType::class, ['attr' => ['maxlength' => 255]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formations::class,
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}
