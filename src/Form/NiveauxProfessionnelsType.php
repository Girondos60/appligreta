<?php

namespace App\Form;

use App\Entity\NiveauxProfessionnels;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NiveauxProfessionnelsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('libNiveau', TextType::class, ['attr' => [
            'placeholder' => "ex:titre professionnel niveau 3",
        ], 'label' => "Niveau Professionnel *"])
        
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NiveauxProfessionnels::class,
        ]);
    }
}
