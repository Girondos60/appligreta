<?php

namespace App\Form;

use App\Entity\Fiches;
use App\Entity\Sessions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FicheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('session' , EntityType::class , [
              "mapped" => false,
              "class" => Sessions::class,
              'query_builder' => function (EntityRepository $er ) use ($options) {
                  return $er->createQueryBuilder('s')
                      ->where('s.formation = :formation')
                      ->setParameter('formation', $options['formation']);
              },
            ])
            ->add('nomNaiss')
            ->add('nationalite')
            ->add('depNaiss')
            ->add('villNaiss')
            ->add('paysNaiss')
            ->add('dureeTransport')
            ->add('nbEnfants')
            ->add('numSecu')
            ->add('cIdent')
            ->add('valIdent')
            ->add('nomUrg')
            ->add('telUrg')
            ->add('lienParente')
            ->add('sortScol')
            ->add('benefForm')
            ->add('nomForm')
            ->add('dateDernForm')
            ->add('adresse')
            ->add('cp')
            ->add('ville')
            ->add('spAdresse')
            ->add('spPoste')
            ->add('identPE')
            ->add('datePE')
            ->add('agencePE')
            ->add('autre')
            ->add('emploi1')
            ->add('emploi2')
            ->add('cpf')
            ->add('mntCpf')
            ->add('handicap')
            ->add('admNomForm')
            ->add('admDebForm')
            ->add('admFinForm')
            ->add('numProgre')
            ->add('nomContact')
            ->add('permis')
            ->add('transport')
            ->add('situationFamiliale')
            ->add('dispositif')
            ->add('niveauFormation')
            ->add('remuneration')
            ->add('spCategorie')
            ->add('typesIndemnite')
            ->add('dernCertif')
            ->add('niveauxCertif')
            ->add('dernClasse')
            ->add('contact')
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fiches::class,
            'formation' => null,
        ]);
    }
}
