<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Salles;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;

class CoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', DateTimeType::class, [
                'date_widget' => 'single_text'
            ])
            ->add('end', DateTimeType::class, [
                'date_widget' => 'single_text'
            ])
            ->add('background_color', ColorType::class)
            ->add('text_color', ColorType::class)
            ->add('border_color', ColorType::class)
            ->add('info')
            ->add('all_day')
            ->add('agenda')
            ->add('user', EntityType::class, array(
                'class' => User::class,
                'query_builder'=>function(EntityRepository $er){
                    return $er->createQueryBuilder('u')
                    ->andWhere('u.roles LIKE :val')
                    ->setParameter('val', '%ROLE_FORMATEUR%');
                }
            ))
            ->add('salles')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cours::class,
        ]);
    }
}