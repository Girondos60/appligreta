<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\CallbackTransformer;
           


class UserType extends AbstractType
/**
 * {@inheritdoc}
 */
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('civilite', ChoiceType::class,[
                'choices'=>['Madame'=>'Mme', 'Monsieur'=>'Mr'],
                
                
            ])
            ->add('nomUsage')
            ->add('prenom')
            ->add('dateNaiss',DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de Naissance',
                'attr' => ['class'=>'js-datepicker'],
            ])
            ->add('fixe')
            ->add('portable')
            ->add('mail')
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'registration.message.repeated_password_invalid',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de pass: '],
                'second_options' => ['label' => 'Confirme mot de pass: '],
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Votre mot de passe répété est incorrect',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit avoir min 6 caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('roles', null,array( 'attr'=>array('style'=>'display:none;')), ChoiceType::class, [
                'choices' => [
                    'Candidat' => 'ROLE_CANDIDAT',
                    'Formateur' => 'ROLE_FORMATEUR',
                    'Employe' => 'ROLE_EMPLOYE',
                    'Administrateur' => 'ROLE_ADMIN',
                    'SuperAdmin' => 'ROLE_SUPER_ADMIN'
                ],
               
                'multiple' => true,
                'label' => 'Rôles',
               
            ])


            
            ->add('imageFile', VichImageType::class, array (
                'label' => 'Image (.jpg ou .png)',
                'download_link'=> false ,
                'required' => false ,
                'delete_label' => 'Supprimer?'
            ))

            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'J\'accepte les termes et conditions.'
                    ])
                ]
            ])
            
        ;

                    //Data transformer
                    $builder
                    ->get('roles')
                    ->addModelTransformer(new CallbackTransformer(
                        function ($rolesAsArray) {
                            // transform the array to a string
                            return implode(', ',$rolesAsArray);
                        },
                        function ($rolesAsString) {
                            // transform the string back to an array
                            return explode(', ', $rolesAsString);
                        }
                    ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}
