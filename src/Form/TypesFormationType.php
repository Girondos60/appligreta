<?php

namespace App\Form;

use App\Entity\TypesFormation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypesFormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('LibType', TextType::class, ['attr' => [
                'placeholder' => "ex:formation dipmlômantes",
            ], 'label' => "Type de Formation *"])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypesFormation::class,
        ]);
    }
}
