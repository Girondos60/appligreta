<?php

namespace App\Form;

use App\Entity\Sessions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SessionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDeb',DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de debut',
                'attr' => ['class'=>'js-datepicker'],
            ])
            ->add('dateFin',DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de fin',
                'attr' => ['class'=>'js-datepicker'],
            ])
            ->add('titre', TextType::class, ['attr' => [
                'placeholder' => "titre",
            ], 'label' => "Titre *"])
            ->add('formation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sessions::class,
        ]);
    }
}
